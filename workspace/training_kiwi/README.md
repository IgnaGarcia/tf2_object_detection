
#####################################################################################################
# Preparacion del modelo.
#####################################################################################################
# Debemos tener ya preparadas nuestras imagenes con los XML en las carpetas correspondientes.
workspace
	-->training_XXXXX
			-->images
					-->detect: Carpeta con las imagenes que seran utilizadas para detectar objetos por nuestro modelo entrenado.
					-->output: Carpeta con las imagenes de salida con los objetos detectados.
					-->test: Carpeta con imagenes de prueba con sus correspondiente xml.
					-->train: Carpeta con imagenes de entrenamiento con sus correspondiente xml.

# Tener preparado nuestro archivo label_map.pbtxt en la carpeta correspondiente:
workspace
	-->training_XXXXX
			-->annotations: Contendra los archivos .records y label_map.pbtxt

# Descargar un pre-trained-models en https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2_detection_zoo.md
# En este ejemplo usaremos "faster_rcnn_resnet101_v1_640x640_coco" http://download.tensorflow.org/models/object_detection/tf2/20200711/faster_rcnn_resnet101_v1_640x640_coco17_tpu-8.tar.gz
# Lo descomprimimos en la raiz de nuestro proyecto: workspace\training_XXXXX\pre-trained-models
# Crear una carpeta en workspace\training_XXXXX\models con el nombre de nuestro modelo descargado, en este caso "faster_rcnn_resnet101_v1_640x640_coco".
# Ahi mismo debemos copiar el archivo "pipeline.config" que se encuentra dentro del modelo pre-entrenado descargado.
# Debemos configurar el archivo "pipeline.config" segun nuestras necesidades. Para mayor ayuda leer el tutorial del sitio https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/index.html
# Nombramos algunos partes importantes a configurar en el mencionado archivo:
	- num_classes: ***numero de clases etiquetadas aqui***
	- fine_tune_checkpoint: ***path al modelo a pre-entrenado***
	- tf_record_input_reader: ***path al train.record o test.record***
	- label_map_path: ***path al label_map.pbtxt***

# Generar los TFRecords. Abrir consola y dirigirse al interior de nuestro proyecto en el workspace\training_XXXXX\ para ejecutar:
python generate_tfrecord.py -x images/train -l annotations/label_map.pbtxt -o annotations/train.record
python generate_tfrecord.py -x images/test -l annotations/label_map.pbtxt -o annotations/test.record


#####################################################################################################
# Ejecucion del modelo.
#####################################################################################################
# Ejecutar el modelo. Abrir consola y dirigirse al interior de nuestro proyecto en el workspace\training_XXXXX\ para ejecutar:
python model_main_tf2.py --model_dir=models/faster_rcnn_resnet101_v1_640x640_coco --pipeline_config_path=models/faster_rcnn_resnet101_v1_640x640_coco/pipeline.config --logtostderr --checkpoint_every_n=100 --num_workers=2

# Nota: Detallamos dos parametros importantes y su significado:
--checkpoint_every_n=100 Esto indica cada cuantos pasos el modelo realizara un checkpoint.
--num_workers=2 Este parametro indica cuantos nucleos fisicos de CPU deseamos implementar.

# Exportar o congelar un modelo entrenado. Abrir consola y dirigirse al interior de nuestro proyecto en el workspace\training_XXXXX\ para ejecutar:
# Nota: Recordar crear una carpeta en exported-models con el nombre del modelo utilizado, en este ejemplo "faster_rcnn_resnet101_v1_640x640_coco".
python exporter_main_v2.py --input_type=image_tensor --pipeline_config_path=models/faster_rcnn_resnet101_v1_640x640_coco/pipeline.config --trained_checkpoint_dir=models/faster_rcnn_resnet101_v1_640x640_coco/ --output_directory=exported-models/faster_rcnn_resnet101_v1_640x640_coco

# Ejecutar el modelo de deteccion de objetos. Abrir consola y dirigirse al interior de nuestro proyecto en el workspace\training_XXXXX\ para ejecutar:
python make_detection.py -m exported-models/faster_rcnn_resnet101_v1_640x640_coco/ --score 0.7 --inp images/detect --out images/output --l annotations/label_map.pbtxt

# Nota: Detallamos dos parametros importantes y su significado:
-m Indica la ruta al modelo congelado.
--score Indica el porcentaje de confiabilidad minimo de los obejetos detectados.
--inp La ruta con las imagenes a ser detectadas por el modelo.
--out Indica la carpeta donde se alojaran los resultados de la deteccion de objetos.
--l Ruta donde se encuentre el archivo "label_map.pbtxt".


