## Generar los TFRecords. Abrir consola y dirigirse al interior de nuestro proyecto en el workspace\training_XXXXX\ para ejecutar:
```
python generate_tfrecord.py -x images/train -l annotations/label_map.pbtxt -o annotations/train.record
python generate_tfrecord.py -x images/test -l annotations/label_map.pbtxt -o annotations/test.record
```

## Entrenar el modelo. Abrir consola y dirigirse al interior de nuestro proyecto en el workspace\training_XXXXX\ para ejecutar:
```
# {THREADS} = Numero de nucleos de CPU deseados a utilizar

python model_main_tf2.py --model_dir=models/my_faster_rcnn_resnet101_640x640_coco --pipeline_config_path=models/my_faster_rcnn_resnet101_640x640_coco/pipeline.config --checkpoint_every_n=100 --num_workers={THREADS}
```

## Evaluar el modelo:
``` 		### con numpy==1.17.5 ###
python model_main_tf2.py --model_dir=models/my_faster_rcnn_resnet101_640x640_coco --pipeline_config_path=models/my_faster_rcnn_resnet101_640x640_coco/pipeline.config --checkpoint_dir=models/my_faster_rcnn_resnet101_640x640_coco/
```

## Estadisticas del modelo:
```
tensorboard --logdir models/my_faster_rcnn_resnet101_640x640_coco
```

## Exportar o congelar un modelo entrenado. Abrir consola y dirigirse al interior de nuestro proyecto en el workspace\training_XXXXX\ para ejecutar:
```
python exporter_main_v2.py --input_type image_tensor --pipeline_config_path models/my_faster_rcnn_resnet101_640x640_coco/pipeline.config --trained_checkpoint_dir models/my_faster_rcnn_resnet101_640x640_coco/ --output_directory exported-models/my_faster_rcnn_resnet101_640x640_coco
```


## Ejecutar el modelo de deteccion de objetos. Abrir consola y dirigirse al interior de nuestro proyecto en el workspace\training_XXXXX\ para ejecutar:
```
python make_detection.py -m exported-models/my_faster_rcnn_resnet101_640x640_coco/ --score 0.7 --inp images/detect --out images/output --l annotations/label_map.pbtxt
```
