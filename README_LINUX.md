# Deteccion de Objetos con TensorFlow 2.


## Instalacion en plataforma Linux.
[*Link al sitio fuente del tutorial*](https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/index.html)

[*Otro sitio interesante de Tensorflow 2*](https://neptune.ai/blog/how-to-train-your-own-object-detector-using-tensorflow-object-detection-api)


## Estructura de carpetas.
**addons**: Contiene programas o ejecutables de soporte a la utilizacion de Tensorflow 2. Por ejemplo Labelimg para etiquetar imagenes, protobuf, etc.

**models**: Es la carpeta que contiene Tensorflow 2 clonado del repositorio.

**scripts**: Tiene en su interior scripts de ayuda para el pre-procesado. Segun el caso estos scripts pueden copiarse en la carpeta de nuestro modelo a entrenar.

**workspace**: Contendra carpetas con el nombre de nuestro modelo a entrenar. Por norma se pone la palabra training_XXXXX donde las x es el nombre de nuestro modelo, ejemplo training_kiwis

**workspace**
```
	-->training_XXXXX
		-->annotations: Contendra los archivos .records y label_map.pbtxt

		-->exported-models: Contendra sub carpetas con el nombre del modelo que hayamos elegido, ejemplo faster_rcnn_resnet101_v1 y dentro de este se alojara nuestro modelo congelado.

		-->images
			-->detect: Carpeta con las imagenes que seran utilizadas para detectar objetos por nuestro modelo entrenado.
			-->output: Carpeta con las imagenes de salida con los objetos detectados.
			-->test: Carpeta con imagenes de prueba con sus correspondiente xml.
			-->train: Carpeta con imagenes de entrenamiento con sus correspondiente xml.

		-->models: Contendra sub carpetas con el nombre del modelo que hayamos elegido, ejemplo faster_rcnn_resnet101_v1. Es importante que tenga el archivo pipeline.config del modelo pre-entrenado que hayamos descargado.

		-->pre-trained-models: Contendra sub carpetas con el modelo pre-entrenado que hayamos descargado, ejemplo faster_rcnn_resnet101_v1.
```

## Primeros pasos en linux(debian)
- Ejecutar:
```
sudo apt-get autoremove
sudo apt-get autoclean
sudo apt-get update
sudo apt-get -f install
```

## Instalacion Previa.
- Instalar GIT, ejecutando:
```
sudo apt install git
sudo apt autoremove
```
- [Instalar Driver Nvidia (Si tiene placa de video)](https://www.nvidia.com/es-la/)
- [Instalar CUDA (Si tiene placa de video)](https://developer.nvidia.com/cuda-zone)
- [Instalar Anaconda](https://www.anaconda.com/)
- [Descargar ProtoBuf y guardarlo en carpeta apropiada](https://github.com/protocolbuffers/protobuf/releases) o ejecutar:
```
sudo apt install protobuf-compiler
sudo apt autoremove

protoc –version
```

## Preparacion del entorno
- Clonar el repositorio
```
git clone https://gitlab.com/IgnaGarcia/tf2_object_detection
cd tf2_object_detection
```

- Crear enviroment(puede cambiar inta_tf2 por un nombre en especial)
```
conda create -y -n inta_tf2 python=3.9

conda activate inta_tf2
```

- Instalamos dependencias
```
python -m pip install --upgrade pip
pip install numpy
pip install pandas
pip install matplotlib
pip install --ignore-installed --upgrade tensorflow==2.5.0
pip install cython
conda install scipy
```

- Compilar biblioteca de TF2 (Dentro de models/research/)
```
protoc object_detection/protos/*.proto --python_out=.
```
*NOTA: puede que debas agregar {DONDE GUARDASTE PROTOC}/protoc/bin/ a las variables de entorno con:`export PATH={PATH A AGREGAR}:$PATH`*

- Instalar COCO Tools (Dentro de models/research/)
```
mv pycocotools pycocotools_old
pip install tensorflow-object-detection-api

git clone https://github.com/cocodataset/cocoapi.git
cd cocoapi/PythonAPI
make
cp -r pycocotools <PATH_TO_TF>/TensorFlow/models/research/
```

- Instalar ObjectDetection API (Dentro de models/research/)
```
cp object_detection/packages/tf2/setup.py .
python -m pip install --use-feature=2020-resolver .
```

- Exportar variables de entorno (Dentro de models/research/)
```
export PYTHONPATH=$PYTHONPATH:`pwd`:`pwd`/slim
```