# Deteccion de Objetos con TensorFlow

Repositorio del proyecto de deteccion de objetos de botones florales utilizando la biblioteca TensorFlow, a partir de la guia realizada por la comunidad de TensorFlow [aqui](https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/training.html)

## Setup

Pasos a seguir para tener el ambiente configurado y preparado para su uso. Recomiendo seguir [esta guia](https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/training.html)

- Descargar [*LabelImg*](https://tzutalin.github.io/labelImg/) para etiquetar las imagenes

- Tener instalado [*Python*](https://www.python.org/downloads/release/python-388/)(una version entre 3.6 y 3.8)

- Instalar [*TensorFlow*](https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/install.html#tf-install)

- Descargar [*TensorFlow ObjectDetection API*](https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/install.html#tf-models-install)

- Ejecutar en consola:
```
python -m pip install -upgrade pip
pip install numpy
pip install pandas
pip install matplotlib
```

- Descargar un Modelo de Deteccion de Objetos
	- [*TensorFlow 2*](https://github.com/tensorflow/models/blob/master/research/object_detection/g3doc/tf2_detection_zoo.md)



## Uso
- Etiquetar las imagenes y guardarlas en *images/*, agarrar cierto porcentaje y guardarlo en test, y el resto en train(deben estar de a pares .jpg y .xml)

- Dependiendo de las clases etiquetadas editar el .config del modelo descargado y la estructura del directorio
	- num_classes: ***numero de clases etiquetadas aqui***
	- fine_tune_checkpoint: ***path al modelo a entrenar***
	- tf_record_input_reader: ***path al train.record o test.record***
	- label_map_path: ***path al label_map.pbtxt***
	
- En la carpeta config, editar el label_map y labels de acuerdo a sus clases

- Preparamos los .record:
```
# Create train data:
python generate_tfrecord.py -x images/train -l annotations/label_map.pbtxt -o annotations/train.record

# Create test data:
python generate_tfrecord.py -x images/test -l annotations/label_map.pbtxt -o annotations/test.record
```

- Entrenamos el modelo:
```
python model_main_tf2.py --model_dir=models/my_faster_rcnn_resnet101_640x640_coco --pipeline_config_path=models/my_faster_rcnn_resnet101_640x640_coco/pipeline.config
```

- Evaluar el modelo:
```
python model_main_tf2.py --model_dir=models/my_faster_rcnn_resnet101_640x640_coco --pipeline_config_path=models/my_faster_rcnn_resnet101_640x640_coco/pipeline.config --checkpoint_dir=models/my_faster_rcnn_resnet101_640x640_coco/
```

- Estadisticas del modelo:
```
tensorboard --logdir models/my_faster_rcnn_resnet101_640x640_coco
```

- Frizar el modelo:
```
python exporter_main_v2.py --input_type image_tensor --pipeline_config_path models/my_faster_rcnn_resnet101_640x640_coco/pipeline.config --trained_checkpoint_dir models/my_faster_rcnn_resnet101_640x640_coco/ --output_directory exported-models/my_faster_rcnn_resnet101_640x640_coco
```

- Realizar prediccion:
```
python make_detection.py --model exported-models/my_faster_rcnn_resnet101_640x640_coco/ --img_input test-images --label annotations/label_map.pbtxt --img_output test-images/output
```
