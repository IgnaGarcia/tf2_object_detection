
#####################################################################################################
# Deteccion de Objetos con TensorFlow 2.
#####################################################################################################
# Instalacion en plataforma Windows 10.
# Link al sitio fuente del tutorial: https://tensorflow-object-detection-api-tutorial.readthedocs.io/en/latest/index.html
# Otro sitio interesante de Tensorflow 2: https://neptune.ai/blog/how-to-train-your-own-object-detector-using-tensorflow-object-detection-api

#####################################################################################################
# Estructura de carpetas.
#####################################################################################################
addons: Contiene programas o ejecutables de soporte a la utilizacion de Tensorflow 2. Por ejemplo Labelimg para etiquetar imagenes, protobuf, etc.
models: Es la carpeta que contiene Tensorflow 2 clonado del repositorio.
scripts: Tiene en su interior scripts de ayuda para el pre-procesado. Segun el caso estos scripts pueden copiarse en la carpeta de nuestro modelo a entrenar.
workspace: Contendra carpetas con el nombre de nuestro modelo a entrenar. Por norma se pone la palabra training_XXXXX donde las x es el nombre de nuestro modelo, ejemplo training_kiwis

workspace
	-->training_XXXXX
			-->annotations: Contendra los archivos .records y label_map.pbtxt
			-->exported-models: Contendra sub carpetas con el nombre del modelo que hayamos elegido, ejemplo faster_rcnn_resnet101_v1 y dentro de este se alojara nuestro modelo congelado.
			-->images
					-->detect: Carpeta con las imagenes que seran utilizadas para detectar objetos por nuestro modelo entrenado.
					-->output: Carpeta con las imagenes de salida con los objetos detectados.
					-->test: Carpeta con imagenes de prueba con sus correspondiente xml.
					-->train: Carpeta con imagenes de entrenamiento con sus correspondiente xml.
			-->models: Contendra sub carpetas con el nombre del modelo que hayamos elegido, ejemplo faster_rcnn_resnet101_v1. Es importante que tenga el archivo pipeline.config del modelo pre-entrenado que hayamos descargado.
			-->pre-trained-models: Contendra sub carpetas con el modelo pre-entrenado que hayamos descargado, ejemplo faster_rcnn_resnet101_v1.


#####################################################################################################
# Instalacion Previa.
#####################################################################################################

# Instalar Driver Nvidia (Si tiene placa de video) https://www.nvidia.com/es-la/
# Instalar CUDA (Si tiene placa de video) https://developer.nvidia.com/cuda-zone
# Instalar Anaconda https://www.anaconda.com/
# Agregar al PATH de la variable de entorno del usuario y sistema. Las ubicaciones dependen de donde se haya instalado Anaconda:
C:\ProgramData\Anaconda3
C:\ProgramData\Anaconda3\Scripts
C:\ProgramData\Anaconda3\Library\bin
C:\ProgramData\Anaconda3\Library\usr\bin

# Abrir consola y comprobar Anaconda:
python --version 

# Abrir consola y ejecutamos:
pip install --upgrade pip
pip3 install tensorflow
pip3 install numpy
pip3 install pandas
pip3 install matplotlib
pip3 install protobuf

# Ver version de Tensorflow:
pip show tensorflow

# Instalar GIT https://git-scm.com/downloads
# Abrir consola y comprobar GIT:
git --version 

# Instalar visual studio https://visualstudio.microsoft.com/es/

# Instalar Visual C++ build tools https://go.microsoft.com/fwlink/?LinkId=691126

# Instalar VC_redist.x64 https://support.microsoft.com/en-us/topic/the-latest-supported-visual-c-downloads-2647da03-1eea-4433-9aff-95f26a218cc0

# Descargamos la carpeta "models" de Tensorflow (en caso que no lo tengamos) y lo descomprimimos en la raiz de nuestro proyecto renombrando la carpeta como "models":
https://github.com/tensorflow/models

# Descargar Protobuf https://github.com/protocolbuffers/protobuf/releases
# Luego agregar al PATH de la variable de entorno del usuario y sistema donde se haya descomprimido la carpeta. Ejemplo:
C:\Program Files\Protoc-3.15.8-win64
C:\Program Files\Protoc-3.15.8-win64\bin

# Abrir consola y comprobar Protobuf:
protoc --version

# Abrir consola y dirigirse al interior de nuestro proyecto en la siguiente ubicacion /models/research/ para ejecutar:
protoc object_detection/protos/*.proto --python_out=.

# Instalamos pycocotools. Abrir consola y ejecutamos:
pip install cython
pip install git+https://github.com/philferriere/cocoapi.git#subdirectory=PythonAPI

# Instalamos Object Detection API. Abrir consola y dirigirse al interior de nuestro proyecto en la siguiente ubicacion /models/research/ para ejecutar:
# Nota: si los comandos no te los reconoce, entonces utilizar el Windows PowerShell.
# Si no funciona instalar CYGWIN https://www.cygwin.com/
cp object_detection/packages/tf2/setup.py .
python -m pip install .

# Probar si todo se instalo de forma correcta con un script de tf2.
# Abrir consola y dirigirse al interior de nuestro proyecto en la siguiente ubicacion /models/research/ para ejecutar:
python object_detection/builders/model_builder_tf2_test.py

# Si todo salio bien nos deberia mostrar una salida indicando que corrio 20 pruebas y que salteo 1, el tiempo es variable segun hardware.
# Ran 20 tests in 68.510s
# OK (skipped=1)
