# Guia basica para el uso de anaconda

- Para crear el entorno ejecutar **IMPORTANTE PARA LOS SIGUIEBNTES COMANDOS**
```
conda create -n inta_tf2 --file requirements.txt
```

- Para activar el entorno ejecutar
```
conda activate inta_tf2
```

- Para instalar nuevas dependencias al entorno ejecutar
```
conda install <package-name>
```

- Para salvar las dependencias nuevas ejecutar
```
conda list -e > requirements.txt
```

- Para salir del entorno ejecutar
```
conda deactivate
```